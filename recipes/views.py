from django.shortcuts import get_object_or_404, render, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required

def edit_recipe(request, id):
    post = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance = post)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm()

        context = {
            "recipe_post": post,
            "recipe_form": form,
    }
    return render(request, "recipes/edit.html", context)

@login_required
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            return redirect("recipes_list")
    else:
        form = RecipeForm()

    context = {
        "form": form,
    }
    return render(request, "recipes/create.html", context)

def list_recipes(request):
    list_of_recipes = Recipe.objects.all()
    context = {
        "recipes_key": list_of_recipes,
        }
    return render(
        request,
        "recipes/list.html",
        context
    )

@login_required
def my_recipe_list(request):
    list_of_recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipes_key": list_of_recipes,
        }
    return render(
        request,
        "recipes/list.html",
        context
    )



def show_recipe(request, id):
    a_really_cool_recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": a_really_cool_recipe,
    }
    return render(request, "recipes/detail.html", context)
